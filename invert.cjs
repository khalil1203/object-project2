    // Returns a copy of the object where the keys have become the values and the values the keys.
let ans = (obj)=> {
    let data ={};
    if(Array.isArray(obj)){
        return data;
    }
    if(obj == null || obj == undefined){
        return data;
    }
    if (typeof obj != "object"){
        return data;
    }
    for (let index in obj){
        data[obj[index]] = index;
    }

    return data;
    
}

module.exports = ans;
