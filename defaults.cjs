let ans = (obj, defaultProps)=>{
    let data ={};
    if(Array.isArray(obj)){
        return data;
    }
    if(obj == null || obj == undefined){
        return data;
    }
    if (typeof obj != "object"){
        return data;
    }
    if(Array.isArray(defaultProps)){
        return data;
    }
    if(defaultProps == null || defaultProps == undefined){
        return data;
    }
    if (typeof defaultProps != "object"){
        return data;
    }

    for(let index in defaultProps){
        if(obj[index] == undefined){
            obj[index] = defaultProps[index];
        }
    }
    return obj;

}
module.exports = ans;