//function to retrieve names of object's properties
let ans  = (obj)=>{
    let data = [];
    if(obj == null || obj == undefined){
        return data;
    }
    if (typeof obj != "object"){
        return data;
    }

    for(let index in obj){
        data.push(index);
    }
    return data;
}
module.exports = ans;