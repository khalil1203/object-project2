  // Convert an object into a list of [key, value] pairs.
let ans = (obj)=> {
    let data =[];
    if(Array.isArray(obj)){
        return data;
    }
    if(obj == null || obj == undefined){
        return data;
    }
    if (typeof obj != "object"){
        return data;
    }
    for (let index in obj){
        data.push([index,obj[index]])
    }
    return data;
}

module.exports = ans;
