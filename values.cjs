//function to return values of object's own properties
let ans  = (obj)=>{
    let data = [];
    if(obj == null || obj == undefined){
        return data;
    }
    if (typeof obj != "object"){
        return data;
    }

    for(let index in obj){
        data.push(obj[index]);
    }
    return data;
}
module.exports = ans;