  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
let ans = (obj, callBack)=>{
    if(Array.isArray(obj)){
        return {};
    }
    if(typeof obj != "object"){
        return {};
    }
    for(let index in obj){
        if (typeof obj[index]=="string"){
            obj[index] = callBack(obj[index]);
            break;
        }
    }
    return obj;
}
module.exports = ans;
